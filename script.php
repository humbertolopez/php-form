<?php
	//creas tu conexión a una base de datos. importante que esto sea lo primerito, para que la accion mysql_real_escape_string funcione
	$conexion = mysqli_connect("direccionservidor","usuario","contraseña","nombredebasededatos");
	//ahora, le pides qué tipo de set de caracteres usar en tu conexión, para que todo el texto aparezca
	//con los acentos y pueda leerse
	mysqli_set_charset($conexion,"utf8");
	//qué pasará si no hay conexión, mostrar un mensaje de error
	if(!$conexion)
	{
		die('Error, no se pudo conectar '.mysqli_connect_error());
	}
	//para campos de texto normales
	$campo1 = mysql_real_escape_string($conexion,$_POST['campo1']);
	$campo2 = mysql_real_escape_string($conexion,$_POST['campo2']);
	$campo3 = mysql_real_escape_string($conexion,$_POST['campo3']);
	$campo4 = mysql_real_escape_string($conexion,$_POST['campo4']);
	//para checkboxes, estos no necesitan limpiarse porque tú defines el valor
	$check1 = $_POST['check1'];
	//para selects
	$select1 = $_POST['select1'];
	//la condición para que inicie el script. submit es el mismo nombre que debe tener tu botón de enviar en el formulario
	//ademàs, se puede solicitar que alguno de los campos esté lleno para poder procesar el formulario. con
	// $campo1 != '' pides que campo1 no esté vacío para poder iniciar el script
	if( isset( $_POST['submit'] ) && $campo1 != '')
	{
		//si el usuario da click en submit, entonces...
		//ya que quedó configurada tu conexión allá arriba, armas la petición que se usará al momento de grabar en base de datos
		//es importante que sean los mismos nombres de las columnas que uses en la base que tú creaste. además, ponlas todas
		//aunque no se llenen con algún dato
		$peticion = "INSERT INTO nombredetabla (id,columna1,columna2,columna3,columna4,columna5,columna6)
					VALUES ('','$campo1','$campo2','$campo3','$campo4','$check1','$select1')";
		//ahora sí, que se graben los datos
		mysqli_query($conexion,$peticion);
		//cierra la conexión y listo
		mysqli_close($conexion);
		//ahora, formemos el e-mail. recomiendo checar el manual de la funcion mail() en php
		//http://de1.php.net/manual/en/function.mail.php
		$correo = 'email a quien enviarle';
		$asunto = 'Asunto del e-mail';
		$encabezado = 'MIME-Version: 1.0' . "\r\n";
		$encabezado .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$encabezado .= 'From: mail remitente';
		//en el mensaje puedes incluir código html, solo necesitas declarar en $encabezado que acepte ese código
		//es importante que aisles las variables con '.$variable.' para que sí se muestren
		$mensaje = '
			<html>
			<head>
				<title>Nuevo mensaje</title>
			</head>
			<body>
				<table>
					<tr>
						<td colspan="2">
							<h1>Nuevo mensaje en formulario</h1>
						</td>
					</tr>
					<tr>
						<td>Campo 1:</td>
						<td>'.$campo1.'</td>
					</tr>
					<tr>
						<td>Campo 2:</td>
						<td>'.$campo2.'</td>
					</tr>
					<tr>
						<td>Campo 3:</td>
						<td>'.$campo3.'</td>
					</tr>
					<tr>
						<td>Campo 4:</td>
						<td>'.$campo4.'</td>
					</tr>
					<tr>
						<td>Check 1:</td>
						<td>'.$check1.'</td>
					</tr>
					<tr>
						<td>Select 1:</td>
						<td>'.$select1.'</td>
					</tr>
				</table>
			</body>
			</html>
		';
		//ahora sí, ya que tienes encabezado, correo, asunto y mensaje envia el mail
		if(mail($correo,$asunto,$mensaje,$encabezado))
		{
			//si el mail se envió correctamente, ahora enviaremos el autoresponder
			//esto es hacer de nuevo lo mismo que se hizo arriba para enviar el mail, pero ahora con los datos y mensaje
			//que se enviará al cliente
			$correoresponder = 'email a quien enviarle autoresponder';
			$asuntoresponder = 'Asunto del autoresponder';
			$encabezadoresponder = 'MIME-Version: 1.0' . "\r\n";
			$encabezadoresponder .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$encabezadoresponder .= 'From: mail remitente';
			$mensajeresponder = '
				<html>
				<head>
					<title>Nuevo mensaje</title>
				</head>
				<body>
					<table>
						<tr>
							<td colspan="2">
								<h1>Gracias por tu mensaje</h1>
								<h2>Estos son los datos que nos enviaste:</h2>
							</td>
						</tr>
						<tr>
							<td>Campo 1:</td>
							<td>'.$campo1.'</td>
						</tr>
						<tr>
							<td>Campo 2:</td>
							<td>'.$campo2.'</td>
						</tr>
						<tr>
							<td>Campo 3:</td>
							<td>'.$campo3.'</td>
						</tr>
						<tr>
							<td>Campo 4:</td>
							<td>'.$campo4.'</td>
						</tr>
						<tr>
							<td>Check 1:</td>
							<td>'.$check1.'</td>
						</tr>
						<tr>
							<td>Select 1:</td>
							<td>'.$select1.'</td>
						</tr>
					</table>
				</body>
				</html>
			';
			//ya que tenemos todo para el autoresponder, enviémoslo
			mail($correoresponder,$asuntoresponder,$mensajeresponder,$encabezadoresponder);
		}
		//ya que por fin envió el autorresponder, enviamos al usuario a la página de gracias
		header('Location: http://url-de-tu-pagina-de-gracias');
	} // aqui se acaba nuestro if isset submit
?>